import React, { Component } from "react";
import SkeletonTemplate from "./SkeletonTemplate";
import FetchProducts from "./FetchProducts";

class CategoryPartOne extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <>
        {!this.props.dataState && !this.props.errorState && <SkeletonTemplate />}
        
        {!this.props.errorState && (
          <FetchProducts
            changeData={this.props.changeData}
            changeError={this.props.changeError}
            productState={this.props.productState}
            errorState={this.props.errorState}
            products={this.props.products}
            getProduct={this.props.getProduct}
            addToCart={this.props.addToCart}
          />
        )}
      </>
    );
  }
}

export default CategoryPartOne;
