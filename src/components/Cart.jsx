import React, { Component } from "react";
import CartPartOne from "./CartPartOne";
import CartPartTwo from "./CartPartTwo";

// import Subtotal from "./Subtotal";

class Cart extends Component {
  constructor(props){
    super(props)
  }
  render() {
    
    return (
      <>
        {/* {this.props.cart.length === 0 && (
          <div className="cart-part-one">Cart is Empty</div>
        )}
        {this.props.cart.length !== 0 && (
          <>
            
          </>
        )} */}
        <CartPartOne
          cart={this.props.cart}
          deleteItem={this.props.deleteItem}
          updateQuantity={this.props.updateQuantity}
        />

        <CartPartTwo
          products={this.props.products}
          cart={this.props.cart}
          addToCart={this.props.addToCart}
          cartTotal={this.props.cartTotal}
          changeDataState={this.props.changeDataState}
          toggleCartPopup={this.props.toggleCartPopup}
          showPopup={this.props.showPopup}
        />
      </>
    );
  }
}

export default Cart;
