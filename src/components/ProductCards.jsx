import React, { Component } from "react";
import { Link } from "react-router-dom";

class ProductCards extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <React.Fragment>
        {this.props.products.map((product) => {
          return (
            <div
              className="card-container"
              key={product.title}
              onClick={() => this.props.getProduct(product.id)}
            >
              <div className="card">
                <div
                  className="edit-card"
                  onClick={() => this.props.getProduct(product.id)}
                >
                  <img src={product.image} />
                  <div className="content-spacing">
                    <h3>{product.title}</h3>
                    <h4>${product.price}</h4>
                    <p>{product.description}</p>
                    <h4>Rating : {product.rating["rate"]}</h4>
                  </div>
                </div>
                <div className="card-button">
                  <Link to={`/product/${product.id}`}>
                    <p
                      className="button"
                      
                    >
                      View Product
                    </p>
                  </Link>
                  <button
                    onClick={(event) => {
                      event.stopPropagation();
                      this.props.addToCart(product);
                    }}
                  >
                    Add to cart
                  </button>
                </div>
              </div>
            </div>
          );
        })}
      </React.Fragment>
    );
  }
}
export default ProductCards;
