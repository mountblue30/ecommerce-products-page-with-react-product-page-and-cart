import React, { Component } from "react";
import companyLogo from "../images/ecomwents-logo.png";
import { Link } from "react-router-dom";

function Navbar() {
  return (
    <nav>
      <Link to="/">
        <img src={companyLogo} alt="" />
      </Link>
      <Link to="/cart">
        <i className="fa-solid fa-cart-shopping fa-3x"></i>
      </Link>
    </nav>
  );
}

export default Navbar;
