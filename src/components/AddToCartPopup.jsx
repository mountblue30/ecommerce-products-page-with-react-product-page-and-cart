import React, { Component } from "react";

class AddToCartPopup extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <>
        {!this.props.popUpDeleteStatus && (
          <div className="cart-popup">
            <div className="cart-popup-inner">
              <h1>Product is added to the cart.</h1>
            </div>
          </div>
        )}
        {this.props.popUpDeleteStatus && (
          <div className="cart-popup">
            <div className="cart-popup-inner delete">
              <h1>Product removed from the cart.</h1>
            </div>
          </div>
        )}
      </>
    );
  }
}

export default AddToCartPopup;

// {
//   /* {this.props.updateConfirmation && (
//           <div className="popup_inner">
//             <h1>Product has been updated successfully</h1>
//             <button onClick={this.props.togglePopup}>Continue</button>
//           </div>
//         )}
//         {!this.props.updateConfirmation && (
//           <div className="popup_inner">
//             <h1>Are you sure you want to update the product.</h1>
//             <button onClick={this.props.updateConfirmationState}>Yes</button>
//             <button onClick={this.props.cancelUpdate}>No</button>
//           </div>
//         )} */
// }