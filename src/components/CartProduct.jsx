import React, { Component } from "react";

class CartProduct extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <>
        {!this.props.emptyCart && (
          <div className="product">
            <div className="section-one">
              <div className="image">
                <img src={this.props.product.image} alt="" />
              </div>
              <div className="details-card">
                <h3>{this.props.product.title}</h3>
                <p>{this.props.product.description}</p>
                <h4>Rating : {this.props.product.rating["rate"]}</h4>
                <div className="edit-cart">
                  <div className="product-quantity">
                    <i
                      onClick={() =>
                        this.props.updateQuantity(
                          this.props.product.id,
                          "remove"
                        )
                      }
                      className="fa-solid fa-minus"
                      name="minus"
                    ></i>
                    <h3>{this.props.product.quantity}</h3>
                    <i
                      onClick={() =>
                        this.props.updateQuantity(this.props.product.id, "add")
                      }
                      className="fa-solid fa-plus"
                      name="add"
                    ></i>
                  </div>

                  <button
                    onClick={() =>
                      this.props.deleteItem(
                        this.props.product
                      )
                    }
                  >
                    Delete
                  </button>
                </div>
              </div>
            </div>
            <div className="section-two">
              <h1> $ {this.props.product.price}</h1>
            </div>
          </div>
        )}

        {this.props.emptyCart && (
          <h1 className="empty-cart-title">Please add a product!</h1>
        )}
      </>
    );
  }
}

export default CartProduct;
