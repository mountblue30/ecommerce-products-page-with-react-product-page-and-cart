import React, { Component } from "react";
import CategoryPartOne from "./CategoryPartOne";
import CategoryPartTwo from "./CategoryPartTwo";
import Cart from "./Cart";
import ProductDetails from "./ProductDetails";
import AddToCartPopup from "./AddToCartPopup";
import "../css/cartPage.css";

class Category extends Component {
  state = {
    dataState: false,
    errorState: false,
    productState: false,
    showPopup: false,
    showCartPopup:false,
    updateConfirmation: false,
    formStatus: false,
    products: "",
    productId: "",
    currentProduct: "",
    temporaryProduct: {},
    cart: [],
    cartTotal: 0,
    popUpDeleteStatus: false,
  };
  getExistingProduct = (id) => {
    return this.state.cart.findIndex((product) => product.id === id);
  };
  getOtherProducts = (id) => {
    return this.state.cart.filter((item) => {
      return item.id !== id;
    });
  };
  toggleCartPopup = () => {
    if (this.state.showCartPopup) {
      this.setState((state) => {
        return {
          showCartPopup: !state.showCartPopup,
        };
      });
    } else {
      this.setState(
        (state) => {
          return {
            showCartPopup: !state.showCartPopup,
          };
        },
        () => {
          setTimeout(() => {
            this.toggleCartPopup();
          }, 1000);
        }
      );
    }
  };

  updateQuantity = (id, action) => {
    let productIndex = this.getExistingProduct(id);
    let product = this.state.cart[productIndex];
    if (action === "add") {
      product["quantity"] += 1;
      product["currentPrice"] = +((product["price"] + product["currentPrice"]).toFixed(2));
      this.setState(
        (prevState) => {
          return {
            cart: [
              ...prevState.cart.slice(0, productIndex),
              product,
              ...prevState.cart.slice(productIndex + 1),
            ],
            cartTotal: prevState.cartTotal + product["price"],
            popUpDeleteStatus: false,
          };
        },
        () => {
          this.toggleCartPopup();
        }
      );
    } else {
      if (product.quantity > 1) {
        product.quantity -= 1;
        product["currentPrice"] = +(
          product["currentPrice"] - product["price"]
        ).toFixed(2);
        
        this.setState(
          (prevState) => {
            return {
              cart: [
                ...prevState.cart.slice(0, productIndex),
                product,
                ...prevState.cart.slice(productIndex + 1),
              ],
              cartTotal: prevState.cartTotal - product["price"],
              popUpDeleteStatus: true,
            };
          },
          () => {
            this.toggleCartPopup();
          }
        );
      } else if (product.quantity === 1) {
        product["currentPrice"] = 0;
        if (this.state.cart.length === 0) {
          this.setState({
            cartTotal: 0,
          });
        }  
        this.setState(
          (prevState) => {
            return {
              cartTotal: prevState.cartTotal - product["price"],
              cart: [
                ...prevState.cart.slice(0, productIndex),
                product,
                ...prevState.cart.slice(productIndex + 1),
              ],
              popUpDeleteStatus: true,
            };
          },
          () => {
            this.deleteItem(product);
          }
        );
      }
    }
  };
  addToCart = (product) => {
    let checkExistingProduct = this.getExistingProduct(product.id);
    if (checkExistingProduct !== -1) {
      this.updateQuantity(product.id, "add");
    } else {
      product["quantity"] = 1;
      product["currentPrice"] = product["price"];
      this.setState(
        (prevState) => ({
          cart: [...prevState.cart, product],
          cartTotal: prevState.cartTotal + product["price"],
          popUpDeleteStatus:false,
        }),
        () => {
          this.toggleCartPopup();
          console.log("new",this.state.cart);
        }
      );
    }
  };
  deleteItem = (product) => {
    let productIndex = this.getExistingProduct(product.id);
    console.log(product["currentPrice"]);
    // if(this.state.cart.length === 0){
    //   this.setState({
    //     cartTotal:0
    //   })
    // }    
    this.setState(
      (prevState) => {
        return {
          cartTotal: prevState.cartTotal - product["currentPrice"],
          cart: [
            ...prevState.cart.slice(0, productIndex),
            ...prevState.cart.slice(productIndex + 1),
          ],
          popUpDeleteStatus: true,
        };
      },
      () => {
        this.toggleCartPopup();
      }
    );
  };
  updateProductDetails = () => {
    this.setState((prevState) => ({
      products: prevState.products.map((product) =>
        product.id === this.state.productId
          ? {
              ...product,
              title: this.state.temporaryProduct["title"],
              description: this.state.temporaryProduct["description"],
              price: this.state.temporaryProduct["price"],
              rating: this.state.temporaryProduct["rating"],
              image: this.state.temporaryProduct["image"],
            }
          : product
      ),
      productId: "",
    }));
  };
  updateConfirmationState = () => {
    this.setState({
      updateConfirmation: true,
    });
  };
  togglePopup = () => {
    if (this.state.showPopup) {
      this.setState(
        (state) => {
          return {
            showPopup: !state.showPopup,
            formStatus: false,
            updateConfirmation: false,
          };
        },
        () => {
          this.updateProductDetails();
        }
      );
    } else {
      this.setState((state) => {
        return {
          showPopup: !state.showPopup,
        };
      });
    }
  };
  cancelUpdate = () => {
    this.setState({
      showPopup: !this.state.showPopup,
    });
  };
  changeDataState = (resultProducts) => {
    if (resultProducts.length > 0) {
      this.setState({
        dataState: true,
        productState: true,
        products: resultProducts,
      });
    } else {
      this.setState({
        dataState: true,
        productState: false,
      });
    }
  };

  changeErrorState = () => {
    this.setState({
      errorState: true,
    });
  };
  getProductId = (productId) => {
    this.setState((state) => {
      return {
        currentProduct: state.products[productId - 1],
        productId: productId,
        temporaryProduct: state.products[productId - 1],
        formStatus: true,
      };
    });
  };
  changeFormStatus = () => {
    this.setState({
      formStatus: false,
    });
  };
  handleTitle = (event) => {
    this.setState((prevState) => ({
      temporaryProduct: {
        ...prevState.temporaryProduct,
        title: event.target.value,
      },
    }));
  };
  handleDescription = (event) => {
    this.setState((prevState) => ({
      temporaryProduct: {
        ...prevState.temporaryProduct,
        description: event.target.value,
      },
    }));
  };
  handlePrice = (event) => {
    this.setState((prevState) => ({
      temporaryProduct: {
        ...prevState.temporaryProduct,
        price: event.target.value,
      },
    }));
  };
  handleImage = (event) => {
    this.setState((prevState) => ({
      temporaryProduct: {
        ...prevState.temporaryProduct,
        image: event.target.value,
      },
    }));
  };
  render() {
    return (
      <section className="section-one">
        {this.state.errorState && (
          <div className="errorMessage">
            <h1>An error occured while fetching products!</h1>
          </div>
        )}
        {!this.props.productState && !this.props.errorState && (
          <div className="errorMessage">
            <h1>No products were found!</h1>
          </div>
        )}

        {!this.props.cart &&
          !this.props.product &&
          !this.state.errorState &&
          this.props.productState && (
            <>
              <CategoryPartOne
                dataState={this.state.dataState}
                productState={this.state.productState}
                errorState={this.state.errorState}
                products={this.state.products}
                changeData={this.changeDataState}
                changeError={this.changeErrorState}
                getProduct={this.getProductId}
                addToCart={this.addToCart}
              />
              <CategoryPartTwo
                product={this.state.temporaryProduct}
                dataState={this.state.dataState}
                productState={this.state.productState}
                errorState={this.state.errorState}
                showPopup={this.state.showPopup}
                formStatus={this.state.formStatus}
                updateConfirmation={this.state.updateConfirmation}
                updateConfirmationState={this.updateConfirmationState}
                handleTitle={this.handleTitle}
                handleDescription={this.handleDescription}
                handlePrice={this.handlePrice}
                handleImage={this.handleImage}
                togglePopup={this.togglePopup}
                updateProductDetails={this.updateProductDetails}
                changeFormStatus={this.changeFormStatus}
                inputValidate={this.validate}
                cancelUpdate={this.cancelUpdate}
              />
            </>
          )}
        {this.state.showCartPopup ? (
          <AddToCartPopup
            toggleCartPopup={this.props.toggleCartPopup}
            popUpDeleteStatus={this.state.popUpDeleteStatus}
          />
        ) : null}
        {this.props.product &&
          !this.state.errorState &&
          this.props.productState && (
            <ProductDetails
              item={this.state.cart}
              url={this.props}
              addToCart={this.addToCart}
            />
          )}
        {this.props.cart && !this.state.errorState && this.props.productState && (
          <div className="cart-template">
            <Cart
              cart={this.state.cart}
              products={this.state.products.slice(0, 5)}
              changeDataState={this.changeDataState}
              deleteItem={this.deleteItem}
              updateQuantity={this.updateQuantity}
              addToCart={this.addToCart}
              cartTotal={this.state.cartTotal}
              toggleCartPopup={this.toggleCartPopup}
              showPopup={this.state.showPopup}
            />
          </div>
        )}
      </section>
    );
  }
}

export default Category;
