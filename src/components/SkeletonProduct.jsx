import React, { Component } from "react";
import "../css/productPage.css"
class SkeletonProduct extends Component {
  state = {};
  render() {
    return (
      <div className="prduct-details">
        <div className="image">
          <img className="skeleton" />
        </div>
            <div className="details-card">
              <div className="skeleton skeleton-text"></div>
              <div className="skeleton skeleton-text"></div>
              <div className="skeleton skeleton-text last"></div>
            </div>

            <button>Add to cart</button>
      </div>
    );
  }
}

export default SkeletonProduct;
