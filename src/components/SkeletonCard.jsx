import React, { Component } from "react";

class SkeletonCard extends Component {
  state = {};
  render() {
    return (
      <div className="card-container">
        <div className="card">
          <div className="edit-card">
            <img className="skeleton" />
            <div className="content-spacing">
              <div className="skeleton skeleton-text"></div>
              <div className="skeleton skeleton-text"></div>
              <div className="skeleton skeleton-text last"></div>
            </div>
          </div>
          <div className="card-button">
            <button>View Product</button>
            <button>Add to cart</button>
          </div>
        </div>
      </div>
    );
  }
}

export default SkeletonCard;
