import React, { Component } from "react";
import CartProduct from "./CartProduct";

class CartPartOne extends Component {
  constructor(props){
    super(props)
  }
  state = {};
  render() {
    return (
      <div className="cart-part-one">
        <div className="cart-title">
          <h1>Shopping Cart</h1>
        </div>
        
        {this.props.cart.length > 0 &&
          this.props.cart.map((product) => {
            return (
              <CartProduct
                key={product.id}
                product={product}
                deleteItem={this.props.deleteItem}
                updateQuantity={this.props.updateQuantity}
              />
            );
          })}
        {this.props.cart.length === 0 && <CartProduct emptyCart = {true}/>}
      </div>
    );
  }
}

export default CartPartOne;
