import React, { Component } from 'react';
import axiosBaseUrl from './AxiosBaseUrl';
import SkeletonTemplate from './SkeletonTemplate';
class FetchProductsForCartPage extends Component {
    constructor(props){
        super(props);
        this.state = {
            errorState: false,
            dataState:false,
            products: []
        }
    }
    componentDidMount(){
        this.fetchLimitedData()
    }
    fetchLimitedData() {
        return axiosBaseUrl
          .get("products?limit=5")
          .then((response) => {
            return response.data;
          })
          .then((data) => {
            if(this.props.products === ""){
                this.props.changeDataState(data);
            }
            this.setState({
                products:data,
                dataState:true
            })
          })
          .catch((error) => {
            this.setState({
              errorState: true,
            });
          });
        
    }
    render() { 
        return (
          <>
          {!this.state.dataState && <SkeletonTemplate/>}
            {this.state.errorState && (
              <h1>Error while fetching the products!</h1>
            )}
            {!this.state.errorState &&
              this.state.products.map((product) => {
                return (
                  <>
                    <img src={product.image} />
                    <button onClick={() => this.props.addToCart(product)}>
                      Add to cart
                    </button>
                  </>
                );
              })}
          </>
        );
    }
}
 
export default FetchProductsForCartPage;