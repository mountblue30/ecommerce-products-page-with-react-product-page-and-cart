import React, { Component } from "react";
import SkeletonProduct from "./SkeletonProduct";

class SkeletonTemplateProduct extends Component {
  state = {};
  render() {
    return (
      <div className="section-one-part-two">
        <div className="wrap" id="wrap">
          <div className="contents">
            <SkeletonProduct />
          </div>
        </div>
      </div>
    );
  }
}

export default SkeletonTemplateProduct;
