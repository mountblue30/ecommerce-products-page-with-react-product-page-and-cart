import React, { Component } from "react";
import FetchProductsForCartPage from "./FetchProductsForCartPage";
import AddToCartPopup from "./AddToCartPopup";

class CartPartTwo extends Component {
  constructor(props){
    super(props);
  }
  state = {};
  render() {
    return (
      <>
        <div className="cart-part-two">
          <div className="subtotal">
            <h2>Subtotal({this.props.cart.length} items)</h2>
            {this.props.cart.map((product) => {
              return (
                <>
                  <h2>{product.title.slice(0, 20)}...</h2>
                  <h3>
                    {product.quantity} x $ {product.price} = $
                    {product.currentPrice}
                  </h3>
                </>
              );
            })}
            <h2>Total - $ {this.props.cartTotal.toFixed(2)}</h2>
          </div>
          <div className="other-products" key={1}>
            <FetchProductsForCartPage
              changeDataState={this.props.changeDataState}
              addToCart={this.props.addToCart}
              products={this.props.products}
            />
          </div>
        </div>
      </>
    );
  }
}

export default CartPartTwo;
