import React, { Component } from "react";
import ProductCards from "./ProductCards";

class WrapCards extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <div className="contents">
        <ProductCards
          getProduct={this.props.getProduct}
          products={this.props.data}
          addToCart={this.props.addToCart}
        />
      </div>
    );
  }
}

export default WrapCards;
