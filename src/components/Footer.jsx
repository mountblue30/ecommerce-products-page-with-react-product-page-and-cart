import React, { Component } from 'react';
import companyLogo from "../images/ecomwents-logo.png";

class Footer extends Component {
    state = {  } 
    render() { 
        return (
          <section className="footer">
            <div className="footer-content">
              <div className="footer-part-one">
                <div className="col-1">
                  <div className="logo">
                    <img src={companyLogo} alt=""/>
                  </div>
                  <p>© EcomWents. All Rights Reserved</p>
                </div>
              </div>
            </div>
          </section>
        );
    }
}
 
export default Footer;