import React, { Component } from 'react';
import SkeletonProduct from "./SkeletonProduct";
import axiosBaseUrl from './AxiosBaseUrl';

class ProductDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dataState: false,
      product:""
    };
  }
  componentDidMount(){
    this.fetchSingleProduct()
  }
  fetchSingleProduct() {
    return axiosBaseUrl
      .get(`products/${this.props.url.match.params["id"]}`)
      .then((response) => {
        return response.data;
      })
      .then((data) => {
        this.setState({
          product: data,
          dataState: true,
        });
      })
      .catch((error) => {
        this.setState({
          errorState: true,
        });
      });
  }
  render() {
    
    return (
      <>
      {!this.state.dataState && <SkeletonProduct/>}
        {this.state.dataState && (
          <div className="product-details">
            <div className="image">
              <img
                src={this.state.product.image}
                alt=""
              />
            </div>
            <div className="details-card">
              <h3>{this.state.product["title"]}</h3>
              <h4>Price : $ {this.state.product["price"]}</h4>
              <p>{this.state.product["description"]}</p>
              <h4>Rating : {this.state.product.rating["rate"]}</h4>
              <button onClick={() => this.props.addToCart(this.state.product)}>
                Add to cart
              </button>
            </div>
          </div>
        )}
      </>
    );
  }
}
 
export default ProductDetails;