import React, { Component } from "react";
import ProductCards from "./ProductCards";
import WrapContent from "./WrapContent";

class ProductTemplate extends Component {
  constructor(props){
    super(props);
  }
  render() {
    return (
      <div className="section-one-part-two">
        {Object.entries(this.props.data).map(([title, products]) => {
            
          return (
            <WrapContent
              getProduct={this.props.getProduct}
              addToCart={this.props.addToCart}
              key={title}
              data={products}
              title={title}
            />
          );
        })}
        
      </div>
    );
  }
}

export default ProductTemplate;
