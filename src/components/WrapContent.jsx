import React, { Component } from "react";
import WrapCards from "./WrapCards";

class WrapContent extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <div className="wrap" id="wrap">
        <div className="title">{this.props.title}</div>

        <WrapCards
          getProduct={this.props.getProduct}
          data={this.props.data}
          addToCart={this.props.addToCart}
        />
      </div>
    );
  }
}

export default WrapContent;
