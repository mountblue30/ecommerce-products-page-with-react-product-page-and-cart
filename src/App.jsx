// React
import React, { Component } from "react";

// React-route
import {Route, Switch } from "react-router-dom";

// Components
import Navbar from "./components/Navbar";
import Footer from "./components/Footer";
import Category from "./components/Category";

class App extends Component {
  state = {};
  render() {
    return (
      <>
        <Navbar />
        <Switch>
          <Route path="/" exact component={Category} />
          <Route
            path="/product/:id"
            exact
            render={(props) => <Category {...props} product={true} />}
          />
          <Route
            path="/cart"
            exact
            render={(props) => <Category {...props} cart={true} />}
          />
        </Switch>
        <Footer />
      </>
    );
  }
}

export default App;
